# Open Fusion LED Controller Project #
## This is the Windows Remote code for the project ##
### Links to other repositories: ###
* Main Repository for Documentation and Diagrams: [open-fusion-led-controller-main](https://bitbucket.org/2E0PGS/open-fusion-led-controller-main)
* Core Arduino Repository: [open-fusion-led-controller-arduino](https://bitbucket.org/2E0PGS/open-fusion-led-controller-arduino)
* Web Arduino Repository: [open-fusion-led-controller-web-arduino](https://bitbucket.org/2E0PGS/open-fusion-led-controller-web-arduino)
* Raspberry Pi Repository: [open-fusion-led-controller-raspberrypi](https://bitbucket.org/2E0PGS/open-fusion-led-controller-raspberrypi)
* Windows Remote Repository: [open-fusion-led-controller-win-remote](https://bitbucket.org/2E0PGS/open-fusion-led-controller-win-remote)