﻿namespace OflcWinRemote
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbConnection = new System.Windows.Forms.GroupBox();
            this.rbSerialConnection = new System.Windows.Forms.RadioButton();
            this.rbWebConnection = new System.Windows.Forms.RadioButton();
            this.btnTurnOn = new System.Windows.Forms.Button();
            this.btnTurnOff = new System.Windows.Forms.Button();
            this.cbSelectPort = new System.Windows.Forms.ComboBox();
            this.btnConnect = new System.Windows.Forms.Button();
            this.gbSerial = new System.Windows.Forms.GroupBox();
            this.cbShowSerialResponse = new System.Windows.Forms.CheckBox();
            this.gbControl = new System.Windows.Forms.GroupBox();
            this.rtbSerialResponse = new System.Windows.Forms.RichTextBox();
            this.gbSerialResponse = new System.Windows.Forms.GroupBox();
            this.cbAutoScroll = new System.Windows.Forms.CheckBox();
            this.gbFunctions = new System.Windows.Forms.GroupBox();
            this.gbSmartOnlyFunctions = new System.Windows.Forms.GroupBox();
            this.btnDumbLedMode = new System.Windows.Forms.Button();
            this.btnSmartLedMode = new System.Windows.Forms.Button();
            this.tbSpeed = new System.Windows.Forms.TrackBar();
            this.tbBrightness = new System.Windows.Forms.TrackBar();
            this.btnRandomDroplet = new System.Windows.Forms.Button();
            this.btnNightRider = new System.Windows.Forms.Button();
            this.btnAutoLdr = new System.Windows.Forms.Button();
            this.btnJump3 = new System.Windows.Forms.Button();
            this.btnJump7 = new System.Windows.Forms.Button();
            this.btnFade3 = new System.Windows.Forms.Button();
            this.btnFadeCustomColour = new System.Windows.Forms.Button();
            this.btnPassThrough = new System.Windows.Forms.Button();
            this.btnSolidCustomColour = new System.Windows.Forms.Button();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.btnSetColour = new System.Windows.Forms.Button();
            this.pbColour = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnSetSpeed = new System.Windows.Forms.Button();
            this.btnSetBrightness = new System.Windows.Forms.Button();
            this.gbConnection.SuspendLayout();
            this.gbSerial.SuspendLayout();
            this.gbControl.SuspendLayout();
            this.gbSerialResponse.SuspendLayout();
            this.gbFunctions.SuspendLayout();
            this.gbSmartOnlyFunctions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbSpeed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbBrightness)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbColour)).BeginInit();
            this.SuspendLayout();
            // 
            // gbConnection
            // 
            this.gbConnection.Controls.Add(this.rbSerialConnection);
            this.gbConnection.Controls.Add(this.rbWebConnection);
            this.gbConnection.ForeColor = System.Drawing.Color.Yellow;
            this.gbConnection.Location = new System.Drawing.Point(13, 13);
            this.gbConnection.Name = "gbConnection";
            this.gbConnection.Size = new System.Drawing.Size(149, 75);
            this.gbConnection.TabIndex = 0;
            this.gbConnection.TabStop = false;
            this.gbConnection.Text = "Connection";
            // 
            // rbSerialConnection
            // 
            this.rbSerialConnection.AutoSize = true;
            this.rbSerialConnection.Checked = true;
            this.rbSerialConnection.Location = new System.Drawing.Point(55, 19);
            this.rbSerialConnection.Name = "rbSerialConnection";
            this.rbSerialConnection.Size = new System.Drawing.Size(51, 17);
            this.rbSerialConnection.TabIndex = 1;
            this.rbSerialConnection.TabStop = true;
            this.rbSerialConnection.Text = "Serial";
            this.rbSerialConnection.UseVisualStyleBackColor = true;
            // 
            // rbWebConnection
            // 
            this.rbWebConnection.AutoSize = true;
            this.rbWebConnection.Location = new System.Drawing.Point(7, 20);
            this.rbWebConnection.Name = "rbWebConnection";
            this.rbWebConnection.Size = new System.Drawing.Size(42, 17);
            this.rbWebConnection.TabIndex = 0;
            this.rbWebConnection.Text = "API";
            this.rbWebConnection.UseVisualStyleBackColor = true;
            // 
            // btnTurnOn
            // 
            this.btnTurnOn.ForeColor = System.Drawing.Color.Black;
            this.btnTurnOn.Location = new System.Drawing.Point(6, 19);
            this.btnTurnOn.Name = "btnTurnOn";
            this.btnTurnOn.Size = new System.Drawing.Size(75, 23);
            this.btnTurnOn.TabIndex = 1;
            this.btnTurnOn.Text = "Turn ON";
            this.btnTurnOn.UseVisualStyleBackColor = true;
            this.btnTurnOn.Click += new System.EventHandler(this.btnTurnOn_Click);
            // 
            // btnTurnOff
            // 
            this.btnTurnOff.ForeColor = System.Drawing.Color.Black;
            this.btnTurnOff.Location = new System.Drawing.Point(87, 19);
            this.btnTurnOff.Name = "btnTurnOff";
            this.btnTurnOff.Size = new System.Drawing.Size(75, 23);
            this.btnTurnOff.TabIndex = 2;
            this.btnTurnOff.Text = "Turn OFF";
            this.btnTurnOff.UseVisualStyleBackColor = true;
            this.btnTurnOff.Click += new System.EventHandler(this.btnTurnOff_Click);
            // 
            // cbSelectPort
            // 
            this.cbSelectPort.FormattingEnabled = true;
            this.cbSelectPort.Location = new System.Drawing.Point(18, 20);
            this.cbSelectPort.Name = "cbSelectPort";
            this.cbSelectPort.Size = new System.Drawing.Size(121, 21);
            this.cbSelectPort.TabIndex = 4;
            this.cbSelectPort.Text = "Select port";
            this.cbSelectPort.Click += new System.EventHandler(this.cbSelectPort_Click);
            // 
            // btnConnect
            // 
            this.btnConnect.ForeColor = System.Drawing.Color.Black;
            this.btnConnect.Location = new System.Drawing.Point(159, 20);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(75, 23);
            this.btnConnect.TabIndex = 5;
            this.btnConnect.Text = "Connect";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // gbSerial
            // 
            this.gbSerial.Controls.Add(this.cbShowSerialResponse);
            this.gbSerial.Controls.Add(this.cbSelectPort);
            this.gbSerial.Controls.Add(this.btnConnect);
            this.gbSerial.ForeColor = System.Drawing.Color.Yellow;
            this.gbSerial.Location = new System.Drawing.Point(168, 13);
            this.gbSerial.Name = "gbSerial";
            this.gbSerial.Size = new System.Drawing.Size(256, 75);
            this.gbSerial.TabIndex = 6;
            this.gbSerial.TabStop = false;
            this.gbSerial.Text = "Serial";
            // 
            // cbShowSerialResponse
            // 
            this.cbShowSerialResponse.AutoSize = true;
            this.cbShowSerialResponse.Location = new System.Drawing.Point(18, 47);
            this.cbShowSerialResponse.Name = "cbShowSerialResponse";
            this.cbShowSerialResponse.Size = new System.Drawing.Size(126, 17);
            this.cbShowSerialResponse.TabIndex = 8;
            this.cbShowSerialResponse.Text = "Show serial response";
            this.cbShowSerialResponse.UseVisualStyleBackColor = true;
            this.cbShowSerialResponse.CheckedChanged += new System.EventHandler(this.cbShowSerialResponse_CheckedChanged);
            // 
            // gbControl
            // 
            this.gbControl.Controls.Add(this.btnSetBrightness);
            this.gbControl.Controls.Add(this.btnSetSpeed);
            this.gbControl.Controls.Add(this.label3);
            this.gbControl.Controls.Add(this.label2);
            this.gbControl.Controls.Add(this.tbBrightness);
            this.gbControl.Controls.Add(this.tbSpeed);
            this.gbControl.Controls.Add(this.btnSmartLedMode);
            this.gbControl.Controls.Add(this.btnDumbLedMode);
            this.gbControl.Controls.Add(this.btnTurnOn);
            this.gbControl.Controls.Add(this.btnTurnOff);
            this.gbControl.ForeColor = System.Drawing.Color.Yellow;
            this.gbControl.Location = new System.Drawing.Point(13, 94);
            this.gbControl.Name = "gbControl";
            this.gbControl.Size = new System.Drawing.Size(411, 177);
            this.gbControl.TabIndex = 7;
            this.gbControl.TabStop = false;
            this.gbControl.Text = "Control";
            // 
            // rtbSerialResponse
            // 
            this.rtbSerialResponse.BackColor = System.Drawing.Color.Black;
            this.rtbSerialResponse.ForeColor = System.Drawing.Color.Yellow;
            this.rtbSerialResponse.Location = new System.Drawing.Point(6, 18);
            this.rtbSerialResponse.Name = "rtbSerialResponse";
            this.rtbSerialResponse.ReadOnly = true;
            this.rtbSerialResponse.Size = new System.Drawing.Size(231, 208);
            this.rtbSerialResponse.TabIndex = 8;
            this.rtbSerialResponse.Text = "";
            // 
            // gbSerialResponse
            // 
            this.gbSerialResponse.Controls.Add(this.cbAutoScroll);
            this.gbSerialResponse.Controls.Add(this.rtbSerialResponse);
            this.gbSerialResponse.ForeColor = System.Drawing.Color.Yellow;
            this.gbSerialResponse.Location = new System.Drawing.Point(431, 13);
            this.gbSerialResponse.Name = "gbSerialResponse";
            this.gbSerialResponse.Size = new System.Drawing.Size(243, 258);
            this.gbSerialResponse.TabIndex = 9;
            this.gbSerialResponse.TabStop = false;
            this.gbSerialResponse.Text = "Serial response";
            this.gbSerialResponse.Visible = false;
            // 
            // cbAutoScroll
            // 
            this.cbAutoScroll.AutoSize = true;
            this.cbAutoScroll.Checked = true;
            this.cbAutoScroll.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbAutoScroll.Location = new System.Drawing.Point(7, 233);
            this.cbAutoScroll.Name = "cbAutoScroll";
            this.cbAutoScroll.Size = new System.Drawing.Size(75, 17);
            this.cbAutoScroll.TabIndex = 9;
            this.cbAutoScroll.Text = "Auto scroll";
            this.cbAutoScroll.UseVisualStyleBackColor = true;
            // 
            // gbFunctions
            // 
            this.gbFunctions.Controls.Add(this.label1);
            this.gbFunctions.Controls.Add(this.pbColour);
            this.gbFunctions.Controls.Add(this.btnSetColour);
            this.gbFunctions.Controls.Add(this.btnSolidCustomColour);
            this.gbFunctions.Controls.Add(this.btnPassThrough);
            this.gbFunctions.Controls.Add(this.btnFadeCustomColour);
            this.gbFunctions.Controls.Add(this.btnFade3);
            this.gbFunctions.Controls.Add(this.btnJump7);
            this.gbFunctions.Controls.Add(this.btnJump3);
            this.gbFunctions.Controls.Add(this.btnAutoLdr);
            this.gbFunctions.ForeColor = System.Drawing.Color.Yellow;
            this.gbFunctions.Location = new System.Drawing.Point(13, 277);
            this.gbFunctions.Name = "gbFunctions";
            this.gbFunctions.Size = new System.Drawing.Size(200, 293);
            this.gbFunctions.TabIndex = 10;
            this.gbFunctions.TabStop = false;
            this.gbFunctions.Text = "Functions";
            // 
            // gbSmartOnlyFunctions
            // 
            this.gbSmartOnlyFunctions.Controls.Add(this.btnNightRider);
            this.gbSmartOnlyFunctions.Controls.Add(this.btnRandomDroplet);
            this.gbSmartOnlyFunctions.ForeColor = System.Drawing.Color.Yellow;
            this.gbSmartOnlyFunctions.Location = new System.Drawing.Point(12, 576);
            this.gbSmartOnlyFunctions.Name = "gbSmartOnlyFunctions";
            this.gbSmartOnlyFunctions.Size = new System.Drawing.Size(200, 90);
            this.gbSmartOnlyFunctions.TabIndex = 11;
            this.gbSmartOnlyFunctions.TabStop = false;
            this.gbSmartOnlyFunctions.Text = "Smart only functions";
            // 
            // btnDumbLedMode
            // 
            this.btnDumbLedMode.ForeColor = System.Drawing.Color.Black;
            this.btnDumbLedMode.Location = new System.Drawing.Point(6, 48);
            this.btnDumbLedMode.Name = "btnDumbLedMode";
            this.btnDumbLedMode.Size = new System.Drawing.Size(99, 23);
            this.btnDumbLedMode.TabIndex = 3;
            this.btnDumbLedMode.Text = "Dumb LED Mode";
            this.btnDumbLedMode.UseVisualStyleBackColor = true;
            this.btnDumbLedMode.Click += new System.EventHandler(this.btnDumbLedMode_Click);
            // 
            // btnSmartLedMode
            // 
            this.btnSmartLedMode.ForeColor = System.Drawing.Color.Black;
            this.btnSmartLedMode.Location = new System.Drawing.Point(111, 48);
            this.btnSmartLedMode.Name = "btnSmartLedMode";
            this.btnSmartLedMode.Size = new System.Drawing.Size(101, 23);
            this.btnSmartLedMode.TabIndex = 4;
            this.btnSmartLedMode.Text = "Smart LED Mode";
            this.btnSmartLedMode.UseVisualStyleBackColor = true;
            this.btnSmartLedMode.Click += new System.EventHandler(this.btnSmartLedMode_Click);
            // 
            // tbSpeed
            // 
            this.tbSpeed.Location = new System.Drawing.Point(111, 77);
            this.tbSpeed.Maximum = 400;
            this.tbSpeed.Name = "tbSpeed";
            this.tbSpeed.Size = new System.Drawing.Size(165, 45);
            this.tbSpeed.TabIndex = 5;
            this.tbSpeed.Value = 100;
            // 
            // tbBrightness
            // 
            this.tbBrightness.Location = new System.Drawing.Point(112, 120);
            this.tbBrightness.Maximum = 255;
            this.tbBrightness.Name = "tbBrightness";
            this.tbBrightness.Size = new System.Drawing.Size(164, 45);
            this.tbBrightness.TabIndex = 6;
            this.tbBrightness.Value = 255;
            // 
            // btnRandomDroplet
            // 
            this.btnRandomDroplet.ForeColor = System.Drawing.Color.Black;
            this.btnRandomDroplet.Location = new System.Drawing.Point(8, 20);
            this.btnRandomDroplet.Name = "btnRandomDroplet";
            this.btnRandomDroplet.Size = new System.Drawing.Size(114, 23);
            this.btnRandomDroplet.TabIndex = 0;
            this.btnRandomDroplet.Text = "Random Droplet";
            this.btnRandomDroplet.UseVisualStyleBackColor = true;
            this.btnRandomDroplet.Click += new System.EventHandler(this.btnRandomDroplet_Click);
            // 
            // btnNightRider
            // 
            this.btnNightRider.ForeColor = System.Drawing.Color.Black;
            this.btnNightRider.Location = new System.Drawing.Point(8, 50);
            this.btnNightRider.Name = "btnNightRider";
            this.btnNightRider.Size = new System.Drawing.Size(99, 23);
            this.btnNightRider.TabIndex = 1;
            this.btnNightRider.Text = "Night Rider";
            this.btnNightRider.UseVisualStyleBackColor = true;
            this.btnNightRider.Click += new System.EventHandler(this.btnNightRider_Click);
            // 
            // btnAutoLdr
            // 
            this.btnAutoLdr.ForeColor = System.Drawing.Color.Black;
            this.btnAutoLdr.Location = new System.Drawing.Point(8, 20);
            this.btnAutoLdr.Name = "btnAutoLdr";
            this.btnAutoLdr.Size = new System.Drawing.Size(75, 23);
            this.btnAutoLdr.TabIndex = 0;
            this.btnAutoLdr.Text = "AUTO LDR";
            this.btnAutoLdr.UseVisualStyleBackColor = true;
            this.btnAutoLdr.Click += new System.EventHandler(this.btnAutoLdr_Click);
            // 
            // btnJump3
            // 
            this.btnJump3.ForeColor = System.Drawing.Color.Black;
            this.btnJump3.Location = new System.Drawing.Point(7, 50);
            this.btnJump3.Name = "btnJump3";
            this.btnJump3.Size = new System.Drawing.Size(75, 23);
            this.btnJump3.TabIndex = 1;
            this.btnJump3.Text = "Jump 3";
            this.btnJump3.UseVisualStyleBackColor = true;
            this.btnJump3.Click += new System.EventHandler(this.btnJump3_Click);
            // 
            // btnJump7
            // 
            this.btnJump7.ForeColor = System.Drawing.Color.Black;
            this.btnJump7.Location = new System.Drawing.Point(7, 80);
            this.btnJump7.Name = "btnJump7";
            this.btnJump7.Size = new System.Drawing.Size(75, 23);
            this.btnJump7.TabIndex = 2;
            this.btnJump7.Text = "Jump 7";
            this.btnJump7.UseVisualStyleBackColor = true;
            this.btnJump7.Click += new System.EventHandler(this.btnJump7_Click);
            // 
            // btnFade3
            // 
            this.btnFade3.ForeColor = System.Drawing.Color.Black;
            this.btnFade3.Location = new System.Drawing.Point(7, 110);
            this.btnFade3.Name = "btnFade3";
            this.btnFade3.Size = new System.Drawing.Size(75, 23);
            this.btnFade3.TabIndex = 3;
            this.btnFade3.Text = "Fade 3";
            this.btnFade3.UseVisualStyleBackColor = true;
            this.btnFade3.Click += new System.EventHandler(this.btnFade3_Click);
            // 
            // btnFadeCustomColour
            // 
            this.btnFadeCustomColour.ForeColor = System.Drawing.Color.Black;
            this.btnFadeCustomColour.Location = new System.Drawing.Point(8, 140);
            this.btnFadeCustomColour.Name = "btnFadeCustomColour";
            this.btnFadeCustomColour.Size = new System.Drawing.Size(120, 23);
            this.btnFadeCustomColour.TabIndex = 4;
            this.btnFadeCustomColour.Text = "Fade Custom Colour";
            this.btnFadeCustomColour.UseVisualStyleBackColor = true;
            this.btnFadeCustomColour.Click += new System.EventHandler(this.btnFadeCustomColour_Click);
            // 
            // btnPassThrough
            // 
            this.btnPassThrough.ForeColor = System.Drawing.Color.Black;
            this.btnPassThrough.Location = new System.Drawing.Point(8, 170);
            this.btnPassThrough.Name = "btnPassThrough";
            this.btnPassThrough.Size = new System.Drawing.Size(98, 23);
            this.btnPassThrough.TabIndex = 5;
            this.btnPassThrough.Text = "Pass Through";
            this.btnPassThrough.UseVisualStyleBackColor = true;
            this.btnPassThrough.Click += new System.EventHandler(this.btnPassThrough_Click);
            // 
            // btnSolidCustomColour
            // 
            this.btnSolidCustomColour.ForeColor = System.Drawing.Color.Black;
            this.btnSolidCustomColour.Location = new System.Drawing.Point(8, 199);
            this.btnSolidCustomColour.Name = "btnSolidCustomColour";
            this.btnSolidCustomColour.Size = new System.Drawing.Size(113, 23);
            this.btnSolidCustomColour.TabIndex = 6;
            this.btnSolidCustomColour.Text = "Solid Custom Colour";
            this.btnSolidCustomColour.UseVisualStyleBackColor = true;
            this.btnSolidCustomColour.Click += new System.EventHandler(this.btnSolidCustomColour_Click);
            // 
            // btnSetColour
            // 
            this.btnSetColour.ForeColor = System.Drawing.Color.Black;
            this.btnSetColour.Location = new System.Drawing.Point(100, 243);
            this.btnSetColour.Name = "btnSetColour";
            this.btnSetColour.Size = new System.Drawing.Size(75, 23);
            this.btnSetColour.TabIndex = 7;
            this.btnSetColour.Text = "Set";
            this.btnSetColour.UseVisualStyleBackColor = true;
            this.btnSetColour.Click += new System.EventHandler(this.btnSetColour_Click);
            // 
            // pbColour
            // 
            this.pbColour.BackColor = System.Drawing.Color.Purple;
            this.pbColour.Location = new System.Drawing.Point(62, 243);
            this.pbColour.Name = "pbColour";
            this.pbColour.Size = new System.Drawing.Size(21, 21);
            this.pbColour.TabIndex = 8;
            this.pbColour.TabStop = false;
            this.pbColour.Click += new System.EventHandler(this.pbColour_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Yellow;
            this.label1.Location = new System.Drawing.Point(13, 248);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Colour";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 87);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Speed";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 132);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Brightness";
            // 
            // btnSetSpeed
            // 
            this.btnSetSpeed.ForeColor = System.Drawing.Color.Black;
            this.btnSetSpeed.Location = new System.Drawing.Point(314, 77);
            this.btnSetSpeed.Name = "btnSetSpeed";
            this.btnSetSpeed.Size = new System.Drawing.Size(75, 23);
            this.btnSetSpeed.TabIndex = 10;
            this.btnSetSpeed.Text = "Set";
            this.btnSetSpeed.UseVisualStyleBackColor = true;
            this.btnSetSpeed.Click += new System.EventHandler(this.btnSetSpeed_Click);
            // 
            // btnSetBrightness
            // 
            this.btnSetBrightness.ForeColor = System.Drawing.Color.Black;
            this.btnSetBrightness.Location = new System.Drawing.Point(314, 122);
            this.btnSetBrightness.Name = "btnSetBrightness";
            this.btnSetBrightness.Size = new System.Drawing.Size(75, 23);
            this.btnSetBrightness.TabIndex = 11;
            this.btnSetBrightness.Text = "Set";
            this.btnSetBrightness.UseVisualStyleBackColor = true;
            this.btnSetBrightness.Click += new System.EventHandler(this.btnSetBrightness_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(741, 701);
            this.Controls.Add(this.gbSmartOnlyFunctions);
            this.Controls.Add(this.gbFunctions);
            this.Controls.Add(this.gbSerialResponse);
            this.Controls.Add(this.gbControl);
            this.Controls.Add(this.gbSerial);
            this.Controls.Add(this.gbConnection);
            this.Name = "Form1";
            this.Text = "OFLC Win Remote";
            this.gbConnection.ResumeLayout(false);
            this.gbConnection.PerformLayout();
            this.gbSerial.ResumeLayout(false);
            this.gbSerial.PerformLayout();
            this.gbControl.ResumeLayout(false);
            this.gbControl.PerformLayout();
            this.gbSerialResponse.ResumeLayout(false);
            this.gbSerialResponse.PerformLayout();
            this.gbFunctions.ResumeLayout(false);
            this.gbFunctions.PerformLayout();
            this.gbSmartOnlyFunctions.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tbSpeed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbBrightness)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbColour)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbConnection;
        private System.Windows.Forms.RadioButton rbSerialConnection;
        private System.Windows.Forms.RadioButton rbWebConnection;
        private System.Windows.Forms.Button btnTurnOn;
        private System.Windows.Forms.Button btnTurnOff;
        private System.Windows.Forms.ComboBox cbSelectPort;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.GroupBox gbSerial;
        private System.Windows.Forms.GroupBox gbControl;
        private System.Windows.Forms.CheckBox cbShowSerialResponse;
        private System.Windows.Forms.RichTextBox rtbSerialResponse;
        private System.Windows.Forms.GroupBox gbSerialResponse;
        private System.Windows.Forms.CheckBox cbAutoScroll;
        private System.Windows.Forms.GroupBox gbFunctions;
        private System.Windows.Forms.GroupBox gbSmartOnlyFunctions;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TrackBar tbBrightness;
        private System.Windows.Forms.TrackBar tbSpeed;
        private System.Windows.Forms.Button btnSmartLedMode;
        private System.Windows.Forms.Button btnDumbLedMode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pbColour;
        private System.Windows.Forms.Button btnSetColour;
        private System.Windows.Forms.Button btnSolidCustomColour;
        private System.Windows.Forms.Button btnPassThrough;
        private System.Windows.Forms.Button btnFadeCustomColour;
        private System.Windows.Forms.Button btnFade3;
        private System.Windows.Forms.Button btnJump7;
        private System.Windows.Forms.Button btnJump3;
        private System.Windows.Forms.Button btnAutoLdr;
        private System.Windows.Forms.Button btnNightRider;
        private System.Windows.Forms.Button btnRandomDroplet;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.Button btnSetBrightness;
        private System.Windows.Forms.Button btnSetSpeed;
    }
}

