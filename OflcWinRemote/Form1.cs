﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// Heavily based on the Python Flask web UI: https://bitbucket.org/2E0PGS/open-fusion-led-controller-raspberrypi/src/master/templates/index.html
namespace OflcWinRemote
{
    public partial class Form1 : Form
    {
        private readonly Oflc.Serial.Client.OflcSerialClient _oflcSerialClient;

        public Form1()
        {
            InitializeComponent();
            _oflcSerialClient = new Oflc.Serial.Client.OflcSerialClient();
            UpdateAvailablePortsDropdown();
        }

        private void UpdateAvailablePortsDropdown()
        {
            cbSelectPort.Items.Clear();
            string[] availablePorts = _oflcSerialClient.GetAvailableSerialPorts();
            foreach (string port in availablePorts)
            {
                cbSelectPort.Items.Add(port);
            }
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            if(cbSelectPort.SelectedItem != null)
            {
                if (btnConnect.Text == "Connect")
                {
                    _oflcSerialClient.Open(cbSelectPort.SelectedItem as string);
                    btnConnect.Text = "Disconnect";
                }
                else
                {
                    _oflcSerialClient.Close();
                    btnConnect.Text = "Connect";
                }
            }
            else
            {
                MessageBox.Show("Please select a serial port.");
            }
        }

        private void cbSelectPort_Click(object sender, EventArgs e)
        {
            UpdateAvailablePortsDropdown();
        }

        private void UpdateResponseIfReqiured()
        {
            if(cbShowSerialResponse.Checked == true)
            {
                rtbSerialResponse.Text += _oflcSerialClient.GetResponse();
            }
            if(cbAutoScroll.Checked == true)
            {
                rtbSerialResponse.SelectionStart = rtbSerialResponse.TextLength;
                rtbSerialResponse.ScrollToCaret();
            }
        }

        private void cbShowSerialResponse_CheckedChanged(object sender, EventArgs e)
        {
            if(cbShowSerialResponse.Checked == true)
            {
                gbSerialResponse.Visible = true;
            }
            else
            {
                gbSerialResponse.Visible = false;
            }
        }

        private void btnTurnOn_Click(object sender, EventArgs e)
        {
            _oflcSerialClient.SetPower("1");
            UpdateResponseIfReqiured();
        }

        private void btnTurnOff_Click(object sender, EventArgs e)
        {
            _oflcSerialClient.SetPower("0");
            UpdateResponseIfReqiured();
        }

        private void btnDumbLedMode_Click(object sender, EventArgs e)
        {
            _oflcSerialClient.SetOutput("0");
            UpdateResponseIfReqiured();
        }

        private void btnSmartLedMode_Click(object sender, EventArgs e)
        {
            _oflcSerialClient.SetOutput("1");
            UpdateResponseIfReqiured();
        }

        private void btnAutoLdr_Click(object sender, EventArgs e)
        {
            _oflcSerialClient.SetMode("1");
            UpdateResponseIfReqiured();
        }

        private void btnJump3_Click(object sender, EventArgs e)
        {
            _oflcSerialClient.SetMode("2");
            UpdateResponseIfReqiured();
        }

        private void btnJump7_Click(object sender, EventArgs e)
        {
            _oflcSerialClient.SetMode("3");
            UpdateResponseIfReqiured();
        }

        private void btnFade3_Click(object sender, EventArgs e)
        {
            _oflcSerialClient.SetMode("4");
            UpdateResponseIfReqiured();
        }

        private void btnFadeCustomColour_Click(object sender, EventArgs e)
        {
            _oflcSerialClient.SetMode("5");
            UpdateResponseIfReqiured();
        }

        private void btnPassThrough_Click(object sender, EventArgs e)
        {
            _oflcSerialClient.SetMode("10");
            UpdateResponseIfReqiured();
        }

        private void btnSolidCustomColour_Click(object sender, EventArgs e)
        {
            _oflcSerialClient.SetMode("13");
            UpdateResponseIfReqiured();
        }

        private void btnSetColour_Click(object sender, EventArgs e)
        {
            _oflcSerialClient.SetCustom(pbColour.BackColor.R + ", " + pbColour.BackColor.G + ", " + pbColour.BackColor.B);
            UpdateResponseIfReqiured();
        }

        private void btnRandomDroplet_Click(object sender, EventArgs e)
        {
            _oflcSerialClient.SetMode("11");
            UpdateResponseIfReqiured();
        }

        private void btnNightRider_Click(object sender, EventArgs e)
        {
            _oflcSerialClient.SetMode("12");
            UpdateResponseIfReqiured();
        }

        private void pbColour_Click(object sender, EventArgs e)
        {
            if(colorDialog1.ShowDialog() == DialogResult.OK)
            {
                pbColour.BackColor = colorDialog1.Color;
            }
        }

        private void btnSetSpeed_Click(object sender, EventArgs e)
        {
            _oflcSerialClient.SetTimer(tbSpeed.Value.ToString());
            UpdateResponseIfReqiured();
        }

        private void btnSetBrightness_Click(object sender, EventArgs e)
        {
            _oflcSerialClient.SetBrightness(tbBrightness.Value.ToString());
            UpdateResponseIfReqiured();
        }
    }
}
